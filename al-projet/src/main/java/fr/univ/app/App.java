package fr.univ.app;

import java.io.File;
import java.io.IOException;

import fr.univ.archive.Archivage;
import fr.univ.builder.HeaderFileBuilder;
import fr.univ.builder.ConcreteHeaderFileBuilder;
import fr.univ.extract.ExtractArchive;
import fr.univ.filtering.ConstructorHandlerFilter;
import fr.univ.filtering.HandlerFilter;
import fr.univ.listing.ListArchive;

public class App {
	public static void main(String[] args) throws IOException {

		HeaderFileBuilder headerFileBuilder = new ConcreteHeaderFileBuilder();
		HandlerFilter handlerFilter = ConstructorHandlerFilter.constructHandleFilterFromCommandHandlerFilter("");

		if (args.length < 2) {
			System.out.println("Please specify the command to execute and file path to do perform the command on...");
		} else if (args.length == 2) {
			switch (args[0]) {
			case "archive":
				// archivage
				System.out.println(args[1]);
				Archivage arch = new Archivage(headerFileBuilder, new File(args[1]));
				arch.archive();
				System.out.println("Archiving done...");
				break;
			case "list":
				// listing
				ListArchive listing = new ListArchive(headerFileBuilder, new File(args[1]), handlerFilter, "");
				listing.Listing();
				System.out.println("Listing Done...");
				break;
			default:
				System.out.println("no command available with this name and those arguments please try agin...");
				break;
			}
		} else if (args.length == 3 && args[0].equalsIgnoreCase("extract")) {
			// exctraction
			ExtractArchive extract = new ExtractArchive(headerFileBuilder, new File(args[1]), args[2], handlerFilter);
			extract.extracting();
			System.out.println("Extracting done...");
		} else if (args.length >= 3 && args[0].equalsIgnoreCase("list")) {
			String commandOptionListing = "";
			for (int i = 2; i < args.length; i++) {
				commandOptionListing += " " + args[i];
			}
			// linsting
			HandlerFilter handlerFilterListing = ConstructorHandlerFilter
					.constructHandleFilterFromCommandHandlerFilter(commandOptionListing);
			ListArchive listing = new ListArchive(headerFileBuilder, new File(args[1]), handlerFilterListing,
					commandOptionListing);
			listing.Listing();
			System.out.println("Listing Done...");
		} else if (args.length >= 4 && args[0].equalsIgnoreCase("extract")) {
			// exctraction
			String commandOptionExtracting = "";
			for (int i = 2; i < args.length; i++) {
				commandOptionExtracting += " " + args[i];
			}
			// linsting
			HandlerFilter handlerFilterExtracting = ConstructorHandlerFilter
					.constructHandleFilterFromCommandHandlerFilter(commandOptionExtracting);
			ExtractArchive extract = new ExtractArchive(headerFileBuilder, new File(args[1]), args[2],
					handlerFilterExtracting);
			extract.extracting();
			System.out.println("Extracting done...");
		}else {
			System.out.println("no command available with this name and those arguments please try agin...");
		}
	}
}
