package fr.univ.extract;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import fr.univ.builder.HeaderFileBuilder;
import fr.univ.builder.Director;
import fr.univ.builder.Header;
import fr.univ.filtering.HandlerFilter;

public class ExtractArchive {
	
	private Director director;
	
	private HeaderFileBuilder headerFileBuilder;
	
	private String distPath;
	
	private DataInputStream reader;
	
	private Header header;
	
	private DataOutputStream writer;
	
	private HandlerFilter handlerFilter;
	
	public ExtractArchive(HeaderFileBuilder headerFileBuilder, File file, String dist, HandlerFilter handlerFilter) throws FileNotFoundException {
		this.director = new Director(headerFileBuilder);
		this.headerFileBuilder = headerFileBuilder;
		this.distPath = dist;
		this.reader = new DataInputStream(new FileInputStream(file));
		this.handlerFilter = handlerFilter;
	}
	
	public void extracting() throws IOException {
		String root_path = "";
		byte[] content = null;
		this.director.loadHeaderAndContent(this.reader);
		this.header = this.headerFileBuilder.getHeader();
		root_path = header.getFilePrefix().trim();
		this.makeFile(header, root_path, content);
		do {
			content = this.director.loadHeaderAndContent(this.reader);
			if(content != null) {
				this.header = this.headerFileBuilder.getHeader();
				this.makeFile(this.header,root_path, content);
			}else {
				this.header = null;
			}
		}while(this.header != null);
	}
	
	private void makeFile(Header header, String root_path, byte[] content) throws IOException {
		if(header.getFileFlag() == 0 && this.handlerFilter.requestFiltering(this.header)) {
			File file = new File(this.distPath + File.separator + header.getFilePrefix().trim().replace(root_path, "") + File.separator + header.getFileName().trim());
			file.createNewFile();
			this.writer = new DataOutputStream(new FileOutputStream(file));
			this.writer.write(content);
			this.writer.flush();
			this.writer.close();
		}else if (header.getFileFlag() == 5 && this.handlerFilter.requestFiltering(this.header)) {
			File file = new File(this.distPath + File.separator +header.getFilePrefix().trim().replace(root_path, "") + File.separator + header.getFileName().trim());
			file.mkdir();
		}else if(header.getFileFlag() == 2 && this.handlerFilter.requestFiltering(this.header)) {
			File file = new File(this.distPath + File.separator + header.getFilePrefix().trim().replace(root_path, "") + header.getFileName().trim());
			file.createNewFile();
			this.writer = new DataOutputStream(new FileOutputStream(file));
			this.writer.write(content);
			this.writer.flush();
			this.writer.close();
		}
	}
}
