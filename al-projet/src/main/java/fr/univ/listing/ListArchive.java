package fr.univ.listing;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import fr.univ.builder.HeaderFileBuilder;
import fr.univ.builder.Director;
import fr.univ.builder.Header;
import fr.univ.filtering.HandlerFilter;
import fr.univ.visitor.HeaderVisitor;
import fr.univ.visitor.HeaderVisitorSample;
import fr.univ.visitor.HeaderVisitorVerbose;

public class ListArchive {

	private Director director;

	private HeaderFileBuilder headerFileBuilder;

	private Header header;

	private File tarFile;

	private DataInputStream reader;

	private HandlerFilter handleFilter;

	private String commandOption;

	private HeaderVisitor visitor;

	public ListArchive(HeaderFileBuilder headerFileBuilder, File file, HandlerFilter handleFitler, String CommandOption)
			throws FileNotFoundException {
		this.director = new Director(headerFileBuilder);
		this.headerFileBuilder = headerFileBuilder;
		this.tarFile = file;
		this.reader = new DataInputStream(new FileInputStream(file));
		this.handleFilter = handleFitler;
		this.commandOption = CommandOption;
	}

	public void Listing() throws IOException {
		int status;
		String root_path = "";
		status = this.director.loadHeader(this.reader);
		if (this.commandOption.contains("-v")) {
			this.header = this.headerFileBuilder.getHeader();
			root_path = header.getFilePrefix().trim();
			HeaderVisitor.ROOT_PATH = root_path;
			if(this.handleFilter.requestFiltering(this.header)) {
				this.visitor = new HeaderVisitorVerbose();
				this.visitor.visitHeaderElementDirectory(this.header);
			}
			do {
				status = this.director.loadHeader(this.reader);
				if (status >= 0) {
					this.header = this.headerFileBuilder.getHeader();
					if (this.header.getFileFlag() == 5 && this.handleFilter.requestFiltering(this.header)) {
						this.visitor.visitHeaderElementDirectory(this.header);
					} else if (this.header.getFileFlag() == 2 && this.handleFilter.requestFiltering(this.header)) {
						this.visitor.visitHeaderElementFile(this.header);
					} else if (this.header.getFileFlag() == 0 && this.handleFilter.requestFiltering(this.header)) {
						this.visitor.visitHeaderElementFile(this.header);
					}
				} else {
					this.header = null;
				}
			} while (this.header != null);
		} else {
			this.header = this.headerFileBuilder.getHeader();
			root_path = header.getFilePrefix().trim();
			HeaderVisitor.ROOT_PATH = root_path;
			if(this.handleFilter.requestFiltering(this.header)) {
				this.visitor = new HeaderVisitorSample();
				this.visitor.visitHeaderElementDirectory(this.header);
			}
			do {
				status = this.director.loadHeader(this.reader);
				if (status >= 0) {
					this.header = this.headerFileBuilder.getHeader();
					if (this.header.getFileFlag() == 5 && this.handleFilter.requestFiltering(this.header)) {
						this.visitor.visitHeaderElementDirectory(this.header);
					} else if (this.header.getFileFlag() == 2 && this.handleFilter.requestFiltering(this.header)) {
						this.visitor.visitHeaderElementFile(this.header);
					} else if (this.header.getFileFlag() == 0 && this.handleFilter.requestFiltering(this.header)) {
						this.visitor.visitHeaderElementFile(this.header);
					}
				} else {
					this.header = null;
				}
			} while (this.header != null);
		}
	}

}
