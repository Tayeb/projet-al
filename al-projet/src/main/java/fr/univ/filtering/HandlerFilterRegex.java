package fr.univ.filtering;

import fr.univ.builder.Header;

public class HandlerFilterRegex extends HandlerFilter{

	public HandlerFilterRegex(String value) {
		// TODO Auto-generated constructor stub
		super(value);
	}
	
	@Override
	public boolean requestFiltering(Header header) {
		// TODO Auto-generated method stub
		if(header.getFileName().trim().matches(this.value)) {
			return this.successor.requestFiltering(header);
		}
		return false;
	}

}
