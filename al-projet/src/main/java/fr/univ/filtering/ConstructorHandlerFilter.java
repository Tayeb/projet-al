package fr.univ.filtering;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class ConstructorHandlerFilter {

	public static HandlerFilter constructHandleFilterFromCommandHandlerFilter(String commandOption) {
		
		HandlerFilter handleFilter = new HandlerFilterEndPoint(null);
		
		
		if (commandOption.contains("-n ")) {
			String value = getValueFromCommandOption(commandOption, "-n");
			HandlerFilter filter = new HandlerFilterPathName(value);
			filter.setSuccessor(handleFilter);
			handleFilter = filter;
		}

		if (commandOption.contains("-nx ")) {
			String value = getValueFromCommandOption(commandOption, "-nx");
			HandlerFilter filter = new HandlerFilterRegex(value);
			filter.setSuccessor(handleFilter);
			handleFilter = filter;
		}

		if (commandOption.contains("-u ")) {
			String value = getValueFromCommandOption(commandOption, "-u");
			HandlerFilter filter = new HandlerFilterUName(value); 
			filter.setSuccessor(handleFilter);
			handleFilter = filter;
		}

		if (commandOption.contains("-g ")) {
			String value = getValueFromCommandOption(commandOption, "-g");
			HandlerFilter filter = new HandlerFilterGName(value); 
			filter.setSuccessor(handleFilter);
			handleFilter = filter;
		}
  
		if (commandOption.contains("-ui ")) {
			String value = getValueFromCommandOption(commandOption, "-ui");
			HandlerFilter filter = new HandlerFilterUID(value);
			filter.setSuccessor(handleFilter);
			handleFilter = filter;
		}

		if (commandOption.contains("-gi ")) {
			String value = getValueFromCommandOption(commandOption, "-gi");
			HandlerFilter filter = new HandlerFilterGID(value);
			filter.setSuccessor(handleFilter);
			handleFilter = filter;
		}

		if (commandOption.contains("-sl ")) {
			String value = getValueFromCommandOption(commandOption, "-sl");
			HandlerFilter filter = new HandlerFilterSL(value); 
			filter.setSuccessor(handleFilter);
			handleFilter = filter;
		}

		if (commandOption.contains("-sg ")) {
			String value = getValueFromCommandOption(commandOption, "-sg");
			HandlerFilter filter = new HandlerFilterSG(value); 
			filter.setSuccessor(handleFilter);
			handleFilter = filter;
		}

		return handleFilter;
	}

	private static String getValueFromCommandOption(String commandOption, String option) {
		String value = "";
		int indexOption = commandOption.indexOf(option);
		for (int i = (indexOption + option.length() + 1); i < commandOption.length() && commandOption.charAt(i) != ' '; i++) {
			value += commandOption.charAt(i);
		}
		return value;
	}
}
