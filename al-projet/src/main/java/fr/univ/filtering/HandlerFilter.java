package fr.univ.filtering;

import fr.univ.builder.Header;

public abstract class HandlerFilter {
	
	protected static int count;
	protected HandlerFilter successor;
	protected String value;
	private boolean satisfait;
	

	public HandlerFilter(String value) {
		this.value = value;
	}

	public HandlerFilter getSuccessor() {
		return successor;
	}

	public void setSuccessor(HandlerFilter successor) {
		this.successor = successor;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public boolean isSatisfait() {
		return satisfait;
	}

	public void setSatisfait(boolean satisfait) {
		this.satisfait = satisfait;
	}
	
	public abstract boolean requestFiltering(Header header);
	
}
