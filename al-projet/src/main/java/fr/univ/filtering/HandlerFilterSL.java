package fr.univ.filtering;

import fr.univ.builder.Header;

public class HandlerFilterSL extends HandlerFilter{

	public HandlerFilterSL(String value) {
		// TODO Auto-generated constructor stub
		super(value);
	}
	
	@Override
	public boolean requestFiltering(Header header) {
		// TODO Auto-generated method stub
		if(header.getFileSIZE() < Long.parseLong(this.value)) {
			return this.successor.requestFiltering(header);
		}
		return false;
	}

}
