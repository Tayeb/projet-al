package fr.univ.filtering;

import fr.univ.builder.Header;

public class Test {
	public static void main(String[] args) {
		HandlerFilter handleFilter = ConstructorHandlerFilter.constructHandleFilterFromCommandHandlerFilter("-ui 1000 -gi 1000");
		Header header = new Header();
		header.setFileGID("1000");
		header.setFileUID("1000");
		if(handleFilter.requestFiltering(header))
			System.out.println("Filtring done...");
	}
}
