package fr.univ.filtering;

import fr.univ.builder.Header;

public class HandlerFilterUID extends HandlerFilter{
	
	public HandlerFilterUID(String value) {
		// TODO Auto-generated constructor stub
		super(value);
	}
	
	@Override
	public boolean requestFiltering(Header header) {
		// TODO Auto-generated method stub
		if(header.getFileUID().trim().equalsIgnoreCase(this.value)) {
			return this.successor.requestFiltering(header);
		}
		return false;
	}

}
