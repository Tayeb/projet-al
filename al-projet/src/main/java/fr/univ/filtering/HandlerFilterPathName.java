package fr.univ.filtering;

import fr.univ.builder.Header;

public class HandlerFilterPathName extends HandlerFilter{
	
	public HandlerFilterPathName(String value) {
		// TODO Auto-generated constructor stub
		super(value);
	}
	
	@Override
	public boolean requestFiltering(Header header) {
		// TODO Auto-generated method stub
		if(header.getFilePrefix().trim().startsWith(this.value)) {
			return this.successor.requestFiltering(header);
		}
		return false;
	}

}
