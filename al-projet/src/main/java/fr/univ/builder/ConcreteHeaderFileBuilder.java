package fr.univ.builder;

public class ConcreteHeaderFileBuilder implements HeaderFileBuilder {
	
	
	private Header header;

	public void initHeader() {
		// TODO Auto-generated method stub
		this.header = new Header();
	}

	public void setFileName(String fileName) {
		// TODO Auto-generated method stub
		this.header.setFileName(fileName);
	}

	public void setFilePermission(String filePermission) {
		// TODO Auto-generated method stub
		this.header.setFilePermission(filePermission);
	}

	public void setFileUID(String fileUID) {
		// TODO Auto-generated method stub
		this.header.setFileUID(fileUID);
	}

	public void setFileGID(String fileGID) {
		// TODO Auto-generated method stub
		this.header.setFileGID(fileGID);
	}

	public void setFileSIZE(long fileSize) {
		// TODO Auto-generated method stub
		this.header.setFileSIZE(fileSize);
	}

	public void setFileLastModifiedDate(String fileLastModifiedDate) {
		// TODO Auto-generated method stub
		this.header.setFileLastModifiedDate(fileLastModifiedDate);
	}

	public void setFileCheckSum(String fileCheckSum) {
		// TODO Auto-generated method stub
		this.header.setFileCheckSum(fileCheckSum);
	}

	public void setFileFlag(byte fileFlag) {
		// TODO Auto-generated method stub
		this.header.setFileFlag(fileFlag);
	}

	public void setFileLinkName(String fileLinkName) {
		// TODO Auto-generated method stub
		this.header.setFileLinkName(fileLinkName);
	}

	public void setFileTypeUSTAR(String fileTypeUSTAR) {
		// TODO Auto-generated method stub
		this.header.setFileTypeUSTAR(fileTypeUSTAR);
	}

	public void setFileVersionUSTAR(String fileVersionUSTAR) {
		// TODO Auto-generated method stub
		this.header.setFileVersionUSTAR(fileVersionUSTAR);
	}

	public void setFileUNAME(String fileUNAME) {
		// TODO Auto-generated method stub
		this.header.setFileUNAME(fileUNAME);
	}

	public void setFileGNAME(String fileGNAME) {
		// TODO Auto-generated method stub
		this.header.setFileGNAME(fileGNAME);
	}

	public void setFileDevMAJOR(String fileDevMAJOR) {
		// TODO Auto-generated method stub
		this.header.setFileDevMAJOR(fileDevMAJOR);
	}

	public void setFileDevMINOR(String fileDevMINOR) {
		// TODO Auto-generated method stub
		this.header.setFileDevMINOR(fileDevMINOR);
	}

	public void setFilePrefix(String filePrefix) {
		// TODO Auto-generated method stub
		this.header.setFilePrefix(filePrefix);
	}

	public Header getHeader() {
		// TODO Auto-generated method stub
		return this.header;
	} 
	
	

}
