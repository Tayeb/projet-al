package fr.univ.builder;

import fr.univ.visitor.HeaderVisitor;

public class Header {

	private String fileName; 					// 100  ***
	private String filePermission; 		// 8	***
	private String fileUID; 					// 8	***
	private String fileGID;						// 8	***
	private long   fileSIZE; 					// 12	***
	private String fileLastModifiedDate; 		// 12	***
	private String fileCheckSum; 				// 8
	private byte   fileFlag; 					// 1
	private String fileLinkName; 				// 100	***
	private String fileTypeUSTAR;				// 5	***
	private String fileVersionUSTAR;			// 3	***
	private String fileUNAME; 				// 32	***
	private String fileGNAME; 				// 32	***
	private String fileDevMAJOR; 				// 8	***
	private String fileDevMINOR; 				// 8	***
	private String filePrefix; 				// 155	***
	
	
	public Header() {
		String spaceCaracters = "        ";
		this.fileCheckSum =	spaceCaracters;
		this.fileTypeUSTAR		= "ttttt";
		this.fileVersionUSTAR	= "xxx";
		this.fileDevMAJOR = spaceCaracters;
		this.fileDevMINOR = spaceCaracters;
		
		this.fileLinkName = "";
		this.filePrefix = "";
		for(int i = 0; i < 100; i++) {
			this.fileLinkName += " ";
		}
	}
	
	
	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFilePermission() {
		return filePermission;
	}


	public void setFilePermission(String filePermission) {
		this.filePermission = filePermission;
	}


	public String getFileUID() {
		return fileUID;
	}


	public void setFileUID(String fileUID) {
		this.fileUID = fileUID;
	}


	public String getFileGID() {
		return fileGID;
	}


	public void setFileGID(String fileGID) {
		this.fileGID = fileGID;
	}


	public long getFileSIZE() {
		return fileSIZE;
	}


	public void setFileSIZE(long fileSIZE) {
		this.fileSIZE = fileSIZE;
	}


	public String getFileLastModifiedDate() {
		return fileLastModifiedDate;
	}


	public void setFileLastModifiedDate(String fileLastModifiedDate) {
		this.fileLastModifiedDate = fileLastModifiedDate;
	}


	public String getFileCheckSum() {
		return fileCheckSum;
	}


	public void setFileCheckSum(String fileCheckSum) {
		this.fileCheckSum = fileCheckSum;
	}


	public byte getFileFlag() {
		return fileFlag;
	}


	public void setFileFlag(byte fileFlag) {
		this.fileFlag = fileFlag;
	}


	public String getFileLinkName() {
		return fileLinkName;
	}


	public void setFileLinkName(String fileLinkName) {
		this.fileLinkName = fileLinkName;
	}


	public String getFileTypeUSTAR() {
		return fileTypeUSTAR;
	}


	public void setFileTypeUSTAR(String fileTypeUSTAR) {
		this.fileTypeUSTAR = fileTypeUSTAR;
	}


	public String getFileVersionUSTAR() {
		return fileVersionUSTAR;
	}


	public void setFileVersionUSTAR(String fileVersionUSTAR) {
		this.fileVersionUSTAR = fileVersionUSTAR;
	}


	public String getFileUNAME() {
		return fileUNAME;
	}


	public void setFileUNAME(String fileUNAME) {
		this.fileUNAME = fileUNAME;
	}


	public String getFileGNAME() {
		return fileGNAME;
	}


	public void setFileGNAME(String fileGNAME) {
		this.fileGNAME = fileGNAME;
	}


	public String getFileDevMAJOR() {
		return fileDevMAJOR;
	}


	public void setFileDevMAJOR(String fileDevMAJOR) {
		this.fileDevMAJOR = fileDevMAJOR;
	}


	public String getFileDevMINOR() {
		return fileDevMINOR;
	}


	public void setFileDevMINOR(String fileDevMINOR) {
		this.fileDevMINOR = fileDevMINOR;
	}


	public String getFilePrefix() {
		return filePrefix;
	}


	public void setFilePrefix(String filePrefix) {
		this.filePrefix = filePrefix;
	}	
	
	public void accept(HeaderVisitor visitor) {
		
	}
	

}
