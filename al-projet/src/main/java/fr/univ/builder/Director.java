package fr.univ.builder;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Map;
import java.util.Set;

public class Director {

	private HeaderFileBuilder headerFileBuilder;
	private Map metaInformation;

	public Director(HeaderFileBuilder headerFileBuilder) {
		this.headerFileBuilder = headerFileBuilder;
	}

	public void constructHeader(File file) throws FileNotFoundException, IOException {
		this.metaInformation = (Map) Files.readAttributes(Paths.get(file.getPath()), "unix:*");
		this.headerFileBuilder.initHeader();
		this.headerFileBuilder.setFileName(file.getName());
		this.headerFileBuilder.setFilePermission(
				PosixFilePermissions.toString((Set<PosixFilePermission>) metaInformation.get("permissions")));
		// this.builder.setFileCheckSum(DigestUtils.md5Hex(new FileInputStream(file)));
		this.testFlag();
		this.headerFileBuilder.setFileSIZE((Long) metaInformation.get("size"));
		this.headerFileBuilder.setFileUID(metaInformation.get("uid").toString());
		this.headerFileBuilder.setFileGID(metaInformation.get("gid").toString());
		this.headerFileBuilder.setFileUNAME(metaInformation.get("owner").toString());
		this.headerFileBuilder.setFileGNAME(metaInformation.get("group").toString());
		this.headerFileBuilder.setFileLastModifiedDate(metaInformation.get("lastModifiedTime").toString());
		String prefix = file.getAbsolutePath();
		prefix = prefix.substring(0, prefix.lastIndexOf(File.separator));
		this.headerFileBuilder.setFilePrefix(prefix);
	}

	public int loadHeader(DataInputStream reader) throws FileNotFoundException {
		long skipSize = 0;
		byte flag = 0;
		String field = "";
		int status = 0;
		this.headerFileBuilder.initHeader();
		this.headerFileBuilder.setFileName(field = this.getHeaderField(reader, 100));
		if(field == null) {
			return -1;
		}
		this.headerFileBuilder.setFilePermission(this.getHeaderField(reader, 9));
		this.headerFileBuilder.setFileUID(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileGID(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileSIZE(skipSize = Long.parseLong(this.getHeaderField(reader,12).trim()));
		this.headerFileBuilder.setFileLastModifiedDate(this.getHeaderField(reader, 12));
		this.headerFileBuilder.setFileCheckSum(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileFlag(flag = Byte.parseByte(this.getHeaderField(reader, 1)));
		this.headerFileBuilder.setFileTypeUSTAR(this.getHeaderField(reader, 5));
		this.headerFileBuilder.setFileVersionUSTAR(this.getHeaderField(reader, 3));
		this.headerFileBuilder.setFileUNAME(this.getHeaderField(reader, 32));
		this.headerFileBuilder.setFileGNAME(this.getHeaderField(reader, 32));
		this.headerFileBuilder.setFileDevMAJOR(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileDevMINOR(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileLinkName(this.getHeaderField(reader, 100));
		this.headerFileBuilder.setFilePrefix(this.getHeaderField(reader, 155));
		if(flag == 0) {
			this.getHeaderField(reader, (int)skipSize);
		}
		return status;
	}
	
	public byte[] loadHeaderAndContent(DataInputStream reader) throws FileNotFoundException {
		byte[] content = {};
		long contentSize = 0;
		byte flag = 0;
		String field = "";
		int status = 0;
		this.headerFileBuilder.initHeader();
		this.headerFileBuilder.setFileName(field = this.getHeaderField(reader, 100));
		if(field == null) {
			return null;
		}
		this.headerFileBuilder.setFilePermission(this.getHeaderField(reader, 9));
		this.headerFileBuilder.setFileUID(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileGID(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileSIZE(contentSize = Long.parseLong(this.getHeaderField(reader,12).trim()));
		this.headerFileBuilder.setFileLastModifiedDate(this.getHeaderField(reader, 12));
		this.headerFileBuilder.setFileCheckSum(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileFlag(flag = Byte.parseByte(this.getHeaderField(reader, 1)));
		this.headerFileBuilder.setFileTypeUSTAR(this.getHeaderField(reader, 5));
		this.headerFileBuilder.setFileVersionUSTAR(this.getHeaderField(reader, 3));
		this.headerFileBuilder.setFileUNAME(this.getHeaderField(reader, 32));
		this.headerFileBuilder.setFileGNAME(this.getHeaderField(reader, 32));
		this.headerFileBuilder.setFileDevMAJOR(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileDevMINOR(this.getHeaderField(reader, 8));
		this.headerFileBuilder.setFileLinkName(this.getHeaderField(reader, 100));
		this.headerFileBuilder.setFilePrefix(this.getHeaderField(reader, 155));
		if(flag == 0) {
			content = this.getHeaderField(reader, (int)contentSize).getBytes();
		}
		return content;
	}

	private void testFlag() {
		if (new Boolean((Boolean) metaInformation.get("isRegularFile")) == true) {
			this.headerFileBuilder.setFileFlag((byte) 0);
		} else if (new Boolean((Boolean) metaInformation.get("isSymbolicLink")) == true) {
			this.headerFileBuilder.setFileFlag((byte) 2);
		} else if (new Boolean((Boolean) metaInformation.get("isDirectory")) == true) {
			this.headerFileBuilder.setFileFlag((byte) 5);
		}
	}

	private String getHeaderField(DataInputStream reader, int length) {
		byte[] buffer = new byte[length];
		int dataSize = 0;
		try {
			dataSize = reader.read(buffer, 0, length);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (dataSize != -1) {
			String filed = new String(buffer);
			return filed;
		} else {
			return null;
		}
	}

}
